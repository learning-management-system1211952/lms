// Function to fetch feedback data from the API
async function fetchFeedbackData() {
	try {
		const response = await fetch('https://lms-trio-node-api--ahmedrohailawan.repl.co/feedback');
		if (!response.ok) {
			throw new Error('Failed to fetch feedback data');
		}
		const data = await response.json();
		return data.feedbacks;
	} catch (error) {
		console.error(error);
		return [];
	}
}

async function displayAllFeedbacks() {
	const feedbacks = await fetchFeedbackData();
	const swiperWrapper = document.querySelector('.swiper-wrapper');
  
	// Use documentFragment to construct HTML elements
	const fragment = document.createDocumentFragment();
  
	feedbacks.forEach((feedback) => {
	  const slide = document.createElement('div');
	  slide.classList.add('swiper-slide');
	  slide.innerHTML = `
		<div class="card">
		  <img class="card-image" src="images/user.png" alt="alternative" />
		  <div class="card-body">
			<p class="italic mb-3">${feedback.feedback}</p>
			<p class="testimonial-author">${feedback.name}</p>
		  </div>
		</div>
	  `;
	   fragment.appendChild(slide);
	});
  
	// Append all feedbacks to the swiper wrapper
	swiperWrapper.appendChild(fragment);
  }
window.onload = function () {
	displayAllFeedbacks();
};


(function ($) {
	"use strict";

	/* Navbar Scripts */
	// jQuery to collapse the navbar on scroll
	$(window).on('scroll load', function () {
		if ($(".navbar").offset().top > 60) {
			$(".fixed-top").addClass("top-nav-collapse");
			console.info('scroll');
		} else {
			console.info('scroll');
			$(".fixed-top").removeClass("top-nav-collapse");
		}
	});

	// jQuery for page scrolling feature - requires jQuery Easing plugin
	$(function () {
		$(document).on('click', 'a.page-scroll', function (event) {
			var $anchor = $(this);
			$('html, body').stop().animate({
				scrollTop: $($anchor.attr('href')).offset().top
			}, 600, 'easeInOutExpo');
			event.preventDefault();
			console.info('scroll to top');
		});
	});

	// close menu on click in small viewport
	$('[data-toggle="offcanvas"], .nav-link:not(.dropdown-toggle').on('click', function () {
		$('.offcanvas-collapse').toggleClass('open')
		console.info('toggleDropdown');
	})

	// hover in desktop mode
	function toggleDropdown(e) {
		const _d = $(e.target).closest('.dropdown'),
			_m = $('.dropdown-menu', _d);
		setTimeout(function () {
			const shouldOpen = e.type !== 'click' && _d.is(':hover');
			_m.toggleClass('show', shouldOpen);
			_d.toggleClass('show', shouldOpen);
			$('[data-toggle="dropdown"]', _d).attr('aria-expanded', shouldOpen);
		}, e.type === 'mouseleave' ? 300 : 0);
		console.info('toggleDropdown');
	}
	$('body')
		.on('mouseenter mouseleave', '.dropdown', toggleDropdown)
		.on('click', '.dropdown-menu a', toggleDropdown);


	/* Details Lightbox - Magnific Popup */
	$('.popup-with-move-anim').magnificPopup({
		type: 'inline',
		fixedContentPos: true,
		fixedBgPos: true,
		overflowY: 'auto',
		closeBtnInside: true,
		preloader: false,
		midClick: true,
		removalDelay: 300,
		mainClass: 'my-mfp-slide-bottom'
	});


	/* Card Slider - Swiper */
	var cardSlider = new Swiper('.card-slider', {
		autoplay: {
			delay: 4000,
			disableOnInteraction: false
		},
		loop: true,
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev'
		},
		slidesPerView: 3,
		spaceBetween: 70,
		breakpoints: {
			// when window is <= 767px
			767: {
				slidesPerView: 1
			},
			// when window is <= 1023px
			1023: {
				slidesPerView: 2,
				spaceBetween: 40
			}
		}
	});


	/* Counter - CountTo */
	var a = 0;
	$(window).scroll(function () {
		if ($('#counter').length) { // checking if CountTo section exists in the page, if not it will not run the script and avoid errors	
			var oTop = $('#counter').offset().top - window.innerHeight;
			console.info('counting...');
			if (a == 0 && $(window).scrollTop() > oTop) {
				$('.counter-value').each(function () {
					var $this = $(this),
						countTo = $this.attr('data-count');
					$({
						countNum: $this.text()
					}).animate({
						countNum: countTo
					},
						{
							duration: 2000,
							easing: 'swing',
							step: function () {
								$this.text(Math.floor(this.countNum));
							},
							complete: function () {
								$this.text(this.countNum);
								//alert('finished');
							}
						});
				});
				a = 1;
			}
		}
	});


	/* Move Form Fields Label When User Types */
	// for input and textarea fields
	$("input, textarea").keyup(function () {
		if ($(this).val() != '') {
			$(this).addClass('notEmpty');
			console.info('input data');
		} else {
			$(this).removeClass('notEmpty');
		}
	});


	/* Back To Top Button */
	// create the back to top button
	$('body').prepend('<a href="body" class="back-to-top page-scroll">Back to Top</a>');
	var amountScrolled = 700;
	$(window).scroll(function () {
		if ($(window).scrollTop() > amountScrolled) {
			$('a.back-to-top').fadeIn('500');
			console.info('back to top');
		} else {
			$('a.back-to-top').fadeOut('500');
		}
	});


	/* Removes Long Focus On Buttons */
	$(".button, a, button").mouseup(function () {
		$(this).blur();
	});

	/* Function to get the navigation links for smooth page scroll */
	function getMenuItems() {
		var menuItems = [];
		$('.nav-link').each(function () {
			var hash = $(this).attr('href').substr(1);
			if (hash !== "")
				console.info('links for page scroll');
			menuItems.push(hash);
		})
		return menuItems;
	}

	/* Prevents adding of # at the end of URL on click of non-pagescroll links */
	$('.nav-link').click(function (e) {
		var hash = $(this).attr('href').substr(1);
		if (hash == "")
			e.preventDefault();
	});

	/* Checks page scroll offset and changes active link on page load */
	changeActive();

	/* Change active link on scroll */
	$(document).scroll(function () {
		changeActive();
	});

	/* Function to change the active link */
	function changeActive() {
		const menuItems = getMenuItems();
		$.each(menuItems, function (index, value) {
			var offsetSection = $('#' + value).offset().top;
			var docScroll = $(document).scrollTop();
			var docScroll1 = docScroll + 1;
			console.info('active link changed');

			if (docScroll1 >= offsetSection) {
				$('.nav-link').removeClass('active');
				$('.nav-link[href$="#' + value + '"]').addClass('active');
			}
		});
	}

})(jQuery);

