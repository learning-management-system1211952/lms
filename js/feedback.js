function submitForm() {
    const name = document.getElementById('name').value;
    const feedback = document.getElementById('feedbackid').value;

    if (!name || !feedback) {
    alert('Please provide both name and feedback.');
    return; // Stop further execution if fields are empty
}

    fetch('https://lms-trio-node-api--ahmedrohailawan.repl.co/feedback', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ name, feedback })
    })
        .then(response => {
            if (response.ok) {
                alert('Feedback submitted successfully!');
            } else {
                alert("Failed to submit feedback.");
            }
        })
        .catch(error => {
            console.error('Error:', error);
            alert('Failed to submit feedback. Please try again.');
        });
}