provider "aws" {
  region     = "us-east-1"
}

resource "aws_dynamodb_table" "lms_trio_landing_page" {
  name           = "lms_trio_landing_page"
  billing_mode   = "PROVISIONED"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "Id"
  range_key      = "createdAt"

  attribute {
    name = "Id"
    type = "S"
  }

  attribute {
    name = "createdAt"
    type = "N"
  }

  ttl {
    attribute_name = "TimeToExist"
    enabled        = false
  }

  // Global Secondary Index to include "lastName", "createdAt", "email", and "organization"
  global_secondary_index {
    name               = "SubmissionIndex"
    hash_key           = "Id"
    range_key          = "createdAt"  // Optional range key
    write_capacity     = 10
    read_capacity      = 10
    projection_type    = "INCLUDE"
    non_key_attributes = ["Name", "Feedback", "createdAt"]
  }

  tags = {
    Name        = "dynamodb-table"
    Environment = "Training"
  }
}

output "dynamodb_table_name" {
  value = aws_dynamodb_table.lms_trio_landing_page.name
}